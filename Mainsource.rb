@zone=0;@vloop=0;@steps=0;@player=0;@run=true;@vfunction=0;
def pbField()
	terrain=$game_map.terrain_tag($game_player.x,$game_player.y)
	return terrain
end
def pbCreatePkmn(pkmnrange)
	species=rand(pkmnrange);@wildpkmn.species=species
	@wildpkmn.name=PBSpecies.getName(species)
	@type1=@wildpkmn.type1;@type2=@wildpkmn.type2;
end
def pbCreateZone(name,zone,coins,player,steps,run,audio)
	if $PokemonGlobal.coins > coins
		if Kernel.pbConfirmMessage(_INTL("\\G\ Enter this Zone costs ${1} coins, are you sure you want to enter {2} ?",coins,name))
			pkmnmp3=sprintf("Audio/BGM/%s.mp3",audio);Audio.bgm_play(pkmnmp3,100,100)
			@zone=zone;@steps=steps;@player=player;@run=run;
			$PokemonGlobal.coins-=coins;pbChangePlayer(@player);
			Kernel.pbMessage(_INTL("{1}, Welcome the {2} !",$Trainer.name,name))
			Kernel.pbMessage(_INTL("Catch all the Pokemon you want \nYou have {1} steps",steps+1))
			Events.onStepTaken+=proc {|sender,s|
			if @steps == 0
				$PokemonGlobal.runningShoes=true
				pbChangePlayer(0);@zone=0;@steps=-1;pbMEPlay("Time Flute");
				Kernel.pbMessage(_INTL("The {1} is Over !\nThanks for participating",name))
				Kernel.pbMessage(_INTL("Enjoy your Pokemon {1} !!!",$Trainer.name))
			elsif @steps > 0
				@steps-=1
				$PokemonGlobal.surfing==true ? (pbChangePlayer(@player)) : (pbChangePlayer(@player));
				@run==true ? ($PokemonGlobal.runningShoes=true) : ($PokemonGlobal.runningShoes=false);
				ItemHandlers::UseInField.add(:BICYCLE,proc{|item|next Kernel.pbMessage(_INTL("You Can't use the Bicycle inside the {1}",name))})
			end
			}
		else
			Kernel.pbMessage(_INTL("All right then ..."))
		end
	else
		Kernel.pbMessage(_INTL("\\G\ You need ${1} coins to enter this zone",coins))
	end	
end
def pbCoinGame(name,coins,game)
	if $PokemonGlobal.coins > coins
		if Kernel.pbConfirmMessage(_INTL("\\G\ This game costs ${1} coins, are you sure you want to play {2} ?",coins,name))
			$PokemonGlobal.coins-=coins;
			pbMiniGame(game)
		else
			Kernel.pbMessage(_INTL("All right then ..."))
		end
	else
		Kernel.pbMessage(_INTL("\\G\ You need ${1} coins to enter this game",coins))
	end	
end
def pbMiniGame(game)
	case game
	when 0 then pbMiningGame
	when 1 then pbSlotMachine(1)
	when 2 then 
		cardpkmn=Array.new(20);
		cardpkmn.fill {|i| rand(PBSpecies.maxValue)+1}
		for i in 0..7 
			pbGiveTriadCard(cardpkmn[i],1) 
		end			
		duel=pbTriadDuel("Team Rocket",9,9,nil,nil,0)					
		for i in 0..7 
			$PokemonGlobal.triads.pbDeleteItem(cardpkmn[i],1)	
		end
		duel > 1 ? (winloss="You didn't win, better luck next time") : ($PokemonGlobal.coins+=15;winloss="You have win $15 coins, Congratulations!");
		Kernel.pbMessage(_INTL("{1}",winloss))
	when 3 then pbVoltorbFlip
	end
end
def pbSinglePkmn(pkmnrange,set,opset)
	@vloop=0
	while @vloop <= 0
		pbCreatePkmn(pkmnrange)
		opset==true ? (op1=1;op2=0) : (op1=0;op2=1);
		set.include?(@type1) ? (@vloop=op1) : (@vloop=op2);
	end
end
def pbDoublePkmn(pkmnrange,set1,set2,opset)
	@vloop=0
	while @vloop <= 0
		pbCreatePkmn(pkmnrange)
		opset==true ? (op1=1;op2=0) : (op1=0;op2=1);
		if set1.include?(@type1)
			set2.include?(@type2) ? (@vloop=op1) : (@vloop=op2);
		elsif set1.include?(@type2)
			set2.include?(@type1) ? (@vloop=op1) : (@vloop=op2);
		end
	end	
end
def pbChoosePkmn(name)
	Kernel.pbMessage(_INTL("Choose the Pokemon you want to {1}",name))
	pbChooseNonEggPokemon(1,3);pkmn=pbGetPokemon(1);
	return pkmn
end
def pbPkwhale(option,name)
	case option
	when 0 then	
		pbHealAll;pbMEPlay("Jingle - Item");
		Kernel.pbMessage(_INTL("We've restored your Pokemon to full health."))
	when 1 then
		pkmn=pbChoosePkmn(name);@vfunction=1;
		Kernel.pbMessage(_INTL("Choose the Move you want to Teach your {1}",pkmn.name))
		move=pbChooseMoveList;pbLearnMove(pkmn,move)
		Kernel.pbMessage(_INTL("Your {1} has learned a new move, Enjoy it !",pkmn.name))
	when 2 then
		pkmn=pbChoosePkmn(name);@vfunction=1;
		Kernel.pbMessage("What is the Gender you want for this Pokemon ? ")
		pkgender=Kernel.pbShowCommands((nil),[_INTL("Male"),_INTL("Female")])
		pbSEPlay('increase',100,100)		
		pkgender==0 ? (gender="Male";pkmn.makeMale) : (gender="Female";pkmn.makeFemale);
		Kernel.pbMessage(_INTL("Your {1} is now {2}, Enjoy it!",pkmn.name,gender))
	when 3 then
		pkmn=pbChoosePkmn(name)
		for form in pbGetEvolvedFormData(pkmn.species)
		newspe=form[2]
		end
		if !newspe
			@vfunction=0;
			Kernel.pbMessage(_INTL("Your {1} doesn't have more evolutions",pkmn.name))
		elsif newspe>0
			evo=PokemonEvolutionScene.new;evo.pbStartScreen(pkmn,newspe)
			evo.pbEvolution;evo.pbEndScreen;pkmn.calcStats;@vfunction=1;
		end
	when 4 then
		pkmn=pbChoosePkmn(name)
		newspe=pbGetPreviousForm(pkmn.species)
		if !newspe
			@vfunction=0;
			Kernel.pbMessage(_INTL("Sorry, Your {1} can't de-evolve",pkmn.name))
		elsif newspe==pkmn.species
			@vfunction=0;
			Kernel.pbMessage(_INTL("Sorry, Your {1} can't de-evolve",pkmn.name))
		elsif newspe>0
			evo=PokemonEvolutionScene.new;evo.pbStartScreen(pkmn,newspe)
			evo.pbEvolution;evo.pbEndScreen;pkmn.calcStats;@vfunction=1;
		end
	end
end
def pbInitPW()
	quantity=$PokemonBag.pbQuantity(:COINCASE)
	if quantity == 0
	Kernel.pbReceiveItem(PBItems::COINCASE)
	$PokemonGlobal.coins=99;pbSEPlay('itemlevel',100,100);	
	Kernel.pbMessage(_INTL("Obtained 99 coins to play the Mini Games!"))
	end
end
def pbOption(name,money,option)
	if $Trainer.money > money
		if Kernel.pbConfirmMessage(_INTL("\\G\ This option costs ${1}, are you sure you want to {2} ?",money,name))
			pbPkwhale(option,name)
			if @vfunction==1
				$Trainer.money-=money;
				Kernel.pbMessage(_INTL("\\G\ {1} handed over ${2} in exchange of {3}",$Trainer.name,money,name))
			else
				Kernel.pbMessage(_INTL("The operation could not be completed"))
			end
		else
			Kernel.pbMessage(_INTL("All right then ..."))
		end
	else
		Kernel.pbMessage(_INTL("\\G\ You need ${1} to enter this option",money))
	end	
end
Events.onWildPokemonCreate+=proc {|sender,a|@wildpkmn=a[0]
	@wildpkmn.makeShiny if @zone==1
	if @zone==2
		terrain=pbField()
		case terrain
		when 2 then 
			set1=[3,7,8,16,17];set2=[1,4,5,10,11,12]
			pbDoublePkmn(649,set1,set2,false)
		when 4 then
			set1=[3,7,8,16,17];set2=[1,4,10]
			pbDoublePkmn(649,set1,set2,true)
		when 7 then
			set1=[4,5,7,8,16,17];set2=[11]
			pbDoublePkmn(649,set1,set2,true)
		when 10 then
			set1=[3,7,8,16,17];set2=[5,12]
			pbDoublePkmn(649,set1,set2,true)
		end
	end
	if @zone==3
		terrain=pbField()
		case terrain
		when 2 then
			set=[1,4,5,10,11,12]
			pbSinglePkmn(251,set,false)
		when 4 then
			set=[1,4,10]
			pbSinglePkmn(251,set,true)
		when 7 then
			set=[11]
			pbSinglePkmn(251,set,true)
		when 10 then
			set=[5,12]
			pbSinglePkmn(251,set,true)
		end
	end
}
Events.onAction+=proc {|sender,e|
if Input.trigger?(Input::CTRL)
	if @steps <=0
	pbSEPlay('navopen',100,100)
	option=1;
	while option > 0
		option1=1;option2=1;
		menu=Kernel.pbShowCommands((nil),[_INTL("Pokecenter PC"),_INTL("Pokemon Features"),_INTL("Mini Games"),_INTL("About PokeWhale"),_INTL("Exit")])
		case menu
		when 0 then pbPokeCenterPC
		when 1 then pbSEPlay('navopen',100,100)
			while option1 > 0
				menupf=Kernel.pbShowCommands((nil),[_INTL("Heal All Party"),_INTL("Teach Moves"),_INTL("Change Gender"),_INTL("Force Evolution"),_INTL("Force De-Evolution"),_INTL("Go back")])
				case menupf
				when 0 then pbPkwhale(0,0)
				when 1 then	pbOption("Teach a Move",10000,1)
				when 2 then pbOption("Change Gender",1000,2)
				when 3 then pbOption("Force an Evolution",1000,3)
				when 4 then pbOption("Force De-Evolution",10000,4)
				when 5 then option1=0;pbSEPlay('navclose',100,100)
				end
			end	
		when 2 then pbSEPlay('navopen',100,100)
			while option2 > 0
				optionsa=1;optiongh=1;
				menumg=Kernel.pbShowCommands((nil),[_INTL("Safari Zone"),_INTL("Dark Zone"),_INTL("Retro Zone"),_INTL("Mining Items"),_INTL("Slot Machine"),_INTL("Triple Triad"),_INTL("Voltorb Flip"),_INTL("Go back")])
				case menumg
				when 0
					pbCreateZone("Safari Zone",1,20,2,99,true,"27 - Elvaan Female")
					optionsa=0;option2=0;option=0;
				when 1
					pbCreateZone("Dark Zone",2,20,1,99,false,"47 - Castle Zvahl")
					optionsa=0;option2=0;option=0;
				when 2
					pbCreateZone("Retro Zone",3,10,7,99,true,"GSC-36 - Azelea Town")
					optionsa=0;option2=0;option=0;
				when 3 then pbCoinGame("Mining Items",10,0)
				when 4 then pbCoinGame("Slot Machine",3,1)
				when 5 then pbCoinGame("Triple Triad",10,2)
				when 6 then pbCoinGame("Voltorb Flip",3,3)
				when 7 then option2=0;pbSEPlay('navclose',100,100)
				end
			end	
		when 3 then
		when 4 then option=0;pbSEPlay('navclose',100,100)
		end
	end	
	else
	Kernel.pbMessage(_INTL("You can't enter in the PokeWhale menu when you are playing a Zone",$Trainer.name))
	end
end
}
pbSEPlay('Refresh',100,100)
Kernel.pbMessage(_INTL("{1} ... \nPokeWhale has been loaded to your Game",$Trainer.name))
Kernel.pbMessage(_INTL("You can open it pressing (CTRL + C)",$Trainer.name));pbInitPW()